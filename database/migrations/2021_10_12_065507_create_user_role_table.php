<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserRoleTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_role', function (Blueprint $table) {
            $table->string('id', 32)->primary();
            $table->string('user_id', 32);
            $table->string('role_id', 32);
            $table->timestamps();
        });

        Schema::table('user_role',function (Blueprint $table){
            $table->foreign('user_id')->references('uuid')->on('users');
            $table->foreign('role_id')->references('id')->on('roles');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_role');
        Schema::table('user_role',function (Blueprint $table){
            $table->dropForeign('user_id');
            $table->dropForeign('role_id');
        });
    }
}
