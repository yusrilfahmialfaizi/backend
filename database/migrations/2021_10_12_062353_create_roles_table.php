<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRolesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('roles', function (Blueprint $table) {
            $table->string('id', 32)->primary();
            $table->string('name');
            $table->string('slug');
            $table->integer('deep');
            $table->bigInteger('urut');
            $table->string('parent_id', 32)->nullable();
            $table->timestamps();
        });

        Schema::table('roles',function (Blueprint $table){
            $table->foreign('parent_id')->references('id')->on('roles');
        });

        // DB::statement('Alter Table `roles` ADD foreign key (parent_id) refferences roles(id)');
    }
    
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('roles');
        Schema::table('roles',function (Blueprint $table){
            $table->dropForeign('parent_id');
        });
        // DB::statement('ALTER TABLE roles DROP FOREIGN KEY parent_id;');
    }
}
