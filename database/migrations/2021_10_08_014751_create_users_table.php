<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->string('uuid', 32)->primary();
            $table->unsignedInteger('company_id')->nullable();
            $table->unsignedInteger('contact_id')->nullable();
            $table->unsignedInteger('city_id')->nullable();
            $table->string('name');
            $table->string('email');
            $table->string('password', 60);
            $table->dateTime('last_login')->nullable();
            $table->string('last_login_ip', 191)->nullable();
            $table->unsignedInteger('group_id')->nullable();
            $table->boolean('is_admin')->nullable();
            $table->boolean('is_active')->default(0);
            $table->dateTime('due_date')->nullable();
            $table->string('slug', 30)->nullable();
            $table->text('fcm_token')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
