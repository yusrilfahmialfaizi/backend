<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateGroupRolesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('group_roles', function (Blueprint $table) {
            $table->string('id', 32)->primary();
            $table->string('group_type_id', 32);
            $table->string('role_id', 32);
            $table->timestamps();
        });

        Schema::table('group_roles',function (Blueprint $table){
            $table->foreign('group_type_id')->references('id')->on('group_type');
            $table->foreign('role_id')->references('id')->on('roles');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('group_roles');
        Schema::table('group_role',function (Blueprint $table){
            $table->dropForeign('group_type_id');
            $table->dropForeign('role_id');
        });
    }
}
