-- phpMyAdmin SQL Dump
-- version 5.1.0
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Oct 12, 2021 at 03:34 AM
-- Server version: 5.7.24
-- PHP Version: 7.4.20

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `backend`
--

-- --------------------------------------------------------

--
-- Table structure for table `authors`
--

CREATE TABLE `authors` (
  `uuid` varchar(32) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `github` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `twitter` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `latest_article_published` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `location` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `authors`
--

INSERT INTO `authors` (`uuid`, `name`, `email`, `github`, `twitter`, `latest_article_published`, `location`, `created_at`, `updated_at`) VALUES
('049fb062af964e72b2bf755d902610f4', 'Miss Abbigail Jakubowski V', 'vjohnson@example.net', 'waldo.hackett@example.org', 'vbrakus@example.org', 'Dr.', 'Surabaya', '2021-10-11 04:35:25', '2021-10-11 04:35:25'),
('ff6e64df11434d7f9706fcb78fb45501', 'Mr. Ward Cormier DDS', 'juliet.gottlieb@example.com', 'wjenkins@example.com', 'rupton@example.com', 'Lumen', 'Surabaya', '2021-10-11 04:35:25', '2021-10-11 05:04:16');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2016_06_01_000001_create_oauth_auth_codes_table', 1),
(2, '2016_06_01_000002_create_oauth_access_tokens_table', 1),
(3, '2016_06_01_000003_create_oauth_refresh_tokens_table', 1),
(4, '2016_06_01_000004_create_oauth_clients_table', 1),
(5, '2016_06_01_000005_create_oauth_personal_access_clients_table', 1),
(11, '2021_10_08_014751_create_users_table', 2),
(12, '2021_10_11_040551_create_authors_table', 3);

-- --------------------------------------------------------

--
-- Table structure for table `oauth_access_tokens`
--

CREATE TABLE `oauth_access_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) UNSIGNED DEFAULT NULL,
  `client_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `oauth_access_tokens`
--

INSERT INTO `oauth_access_tokens` (`id`, `user_id`, `client_id`, `name`, `scopes`, `revoked`, `created_at`, `updated_at`, `expires_at`) VALUES
('0401fb98bd87349a479287d3dd8d0b96d7f20588f68c612f613ad54b23d247cfe807cdd0a7e51875', 1, 3, 'users', '[]', 0, '2021-10-08 17:02:54', '2021-10-08 17:02:54', '2022-10-08 17:02:54'),
('06b88c252abbd3eaaa10e69ebf245631c2f174e5b1124d4ef0d31b09f022beed6bc5622368dd8f44', 1, 3, 'users', '[]', 0, '2021-10-08 08:22:38', '2021-10-08 08:22:38', '2022-10-08 08:22:38'),
('07d6afb89356c1d93ceb85056abd54987debd763b4d8d53a31ae27332660317a78081f010a16f562', 1, 3, 'users', '[]', 0, '2021-10-08 17:03:19', '2021-10-08 17:03:19', '2022-10-08 17:03:19'),
('0806d480f8aebf6ecdbd9034fa5e6080fb397f904cbe1100664da8f0831344c2dbdd22ba787de498', NULL, 5, 'users', '[]', 0, '2021-10-12 03:07:06', '2021-10-12 03:07:06', '2022-10-12 03:07:06'),
('08f25abc01fe158e8ae31c7eb7c6d847c242207d47e30f48e3accecfe72a339830cc1f3b0e529e57', 1, 3, 'users', '[]', 0, '2021-10-08 08:40:20', '2021-10-08 08:40:20', '2022-10-08 08:40:20'),
('0a99d45ba02c0a12adc1afa285f02cfd4105e25dcf8f5aee49d629f52bc536cebad674280f184a79', 1, 3, 'users', '[]', 0, '2021-10-08 17:04:03', '2021-10-08 17:04:03', '2022-10-08 17:04:03'),
('0c4a7a821d10480512c7e0ab5af027dc83f0eea3d1e413c588190a15b22fb90196c1c2a4532aca05', NULL, 5, 'users', '[]', 0, '2021-10-11 08:12:45', '2021-10-11 08:12:45', '2022-10-11 08:12:45'),
('0d7e33ea7e881f54f304d788dc5f3cfd4e7341ccff2d691627efe32c4beb95049f7284f8ddad38fd', NULL, 5, 'users', '[]', 0, '2021-10-11 07:38:58', '2021-10-11 07:38:58', '2022-10-11 07:38:58'),
('0e6c6b2085a3317b88347d38439cc5dc48c2f2f629c14272bc6d6d1a17406dd912328613b1e36345', 1, 3, 'users', '[]', 0, '2021-10-08 08:49:56', '2021-10-08 08:49:56', '2022-10-08 08:49:56'),
('13d4559317faac311850e4a05383869b37129c9ecd456bfa3b5f9174730565a434549d9c2a72fe8f', 1, 3, 'users', '[]', 0, '2021-10-08 08:33:59', '2021-10-08 08:33:59', '2022-10-08 08:33:59'),
('1449973aa365e5d7052ce94c30d66101ffcbe6b0097235dde0f91977e1db0e3fa7a11cebda507774', 1, 3, 'users', '[]', 0, '2021-10-08 08:17:00', '2021-10-08 08:17:00', '2022-10-08 08:17:00'),
('16b3f8eaae0b1aeab7057e12efa626f178c4c3a98402706848639ca2da55e270c0512fb7e1e88073', 1, 5, 'users', '[]', 0, '2021-10-09 02:49:30', '2021-10-09 02:49:30', '2022-10-09 02:49:30'),
('1830b24eec68569b306501d119a58099f866fa96636e0d6cd6ad247e6855751e5d93710990e7170c', NULL, 5, 'users', '[]', 0, '2021-10-11 08:21:05', '2021-10-11 08:21:05', '2022-10-11 08:21:05'),
('1aeba71aad37f135df923de657433c156ff8e084aee13857af8fc2439960877f3c513d9ee4d02ae4', 1, 3, 'users', '[]', 0, '2021-10-08 17:10:39', '2021-10-08 17:10:39', '2022-10-08 17:10:39'),
('1b23acc0a81b91454f7729f2c253f2e3acb6bbcf251380ee54c12a4ca1642ae47efdf541b5ff16ef', 1, 5, 'users', '[]', 0, '2021-10-09 04:33:50', '2021-10-09 04:33:50', '2022-10-09 04:33:50'),
('1cdb5b60fe019830942ab6c77e8b563f60a68dd8d6a42dba3a98c9faffcb49d5fe80e443fcfe9c7c', NULL, 5, 'users', '[]', 0, '2021-10-09 07:35:04', '2021-10-09 07:35:04', '2022-10-09 07:35:04'),
('1d1a5f343a5be2bee57bbd8e0a2e9740bdddf1722c5e3040202eebb4a55dcf885a4a38dfec0c844d', 1, 3, 'users', '[]', 0, '2021-10-08 08:29:05', '2021-10-08 08:29:05', '2022-10-08 08:29:05'),
('1d9c4f0b3e8c0e16822bab5523a09eea2af54fd4f0dbf9e796fcba9ae30dc52284941b5b3617ff78', 1, 5, 'users', '[]', 0, '2021-10-09 04:28:19', '2021-10-09 04:28:19', '2022-10-09 04:28:19'),
('1f8cfb3c0c82574ad95d526cd9b5959580e36cee805fc465fd97bf9d74e8a972d8c4fa170f3f0bab', 1, 1, 'users', '[]', 0, '2021-10-08 07:48:09', '2021-10-08 07:48:09', '2022-10-08 07:48:09'),
('2266dcd1b504d13ec7ecfdccc93e02fa8f2d1c93b4a67438df13183b870ffe726de15671d02cbde5', NULL, 5, 'users', '[]', 0, '2021-10-11 07:35:46', '2021-10-11 07:35:46', '2022-10-11 07:35:46'),
('23c5809cb28ad158e9749633a8b2812a8ca17651413c5f45f4faea3ee22f7ffec98a0eed2e9f0d7d', 1, 3, 'users', '[]', 0, '2021-10-08 17:04:09', '2021-10-08 17:04:09', '2022-10-08 17:04:09'),
('285b4df218ba1fe40798ea095b379d4b87a94b7d551563109e904f10247804f027be6708bf64e515', 1, 3, 'users', '[]', 0, '2021-10-08 17:04:07', '2021-10-08 17:04:07', '2022-10-08 17:04:07'),
('29e156770ae161821e1308b801560ba35961b34d72a4e2765173b3dbaee607a7e907f8cdbf7f2345', 1, 3, 'users', '[]', 0, '2021-10-08 17:22:03', '2021-10-08 17:22:03', '2022-10-08 17:22:03'),
('2bb3a4f763b6b587ada98e613cd556dcc56cc27b1ca1fb4c82ba82f1e2ee44151f820c189b3179a1', 1, 3, 'users', '[]', 0, '2021-10-08 08:22:57', '2021-10-08 08:22:57', '2022-10-08 08:22:57'),
('2eafe9597a7b5721c3e97513b0d1eebb94ae8589fb7f18c7b3bf6e347ab3207364c2237844a4e10f', 1, 3, 'users', '[]', 0, '2021-10-08 08:38:28', '2021-10-08 08:38:28', '2022-10-08 08:38:28'),
('2f64b7743dbe6fbe800d63c224e995c1033682ccac4d293200157bba00c56a9bec64b0acf1633602', NULL, 5, 'users', '[]', 0, '2021-10-09 07:29:55', '2021-10-09 07:29:55', '2022-10-09 07:29:55'),
('2fd4b7aeb6a3250a4419b4f3335549cf022a96a8f018f61c1fb9a6c3058b33351553083dbf4a3d15', 1, 1, 'users', '[]', 0, '2021-10-08 07:41:25', '2021-10-08 07:41:25', '2022-10-08 07:41:25'),
('307897e41f54923d887cdf9bf7b1271404d71c7ff8c248fc71c5408d523edd5a271208295b80fdc2', 1, 5, 'users', '[]', 0, '2021-10-09 04:21:10', '2021-10-09 04:21:10', '2022-10-09 04:21:10'),
('30e4c594a77b05ae580eb5c89d80554f64ce073d8c39734f2c025a0c594785f9ed214914ff7ddc7b', 1, 3, 'users', '[]', 0, '2021-10-08 08:38:27', '2021-10-08 08:38:27', '2022-10-08 08:38:27'),
('31bd249d206577d2f923d1e7087d9441c85181f4dfa795e3612ca8096e1047b963f2d8cf378d7279', NULL, 5, 'users', '[]', 0, '2021-10-11 08:49:46', '2021-10-11 08:49:46', '2022-10-11 08:49:46'),
('31e56aec260fc7f3af2b4cc46c236166a8c9ac7aae9a5361319ef023c8a4d8f930bcc10d5770115e', 1, 3, 'users', '[]', 0, '2021-10-08 08:17:00', '2021-10-08 08:17:00', '2022-10-08 08:17:00'),
('38f87cd51708095c6a9db71ffb023df7a62962bb8d39a6533d8cf00cacb019c965d7853cebf39fae', NULL, 5, 'users', '[]', 0, '2021-10-09 07:37:49', '2021-10-09 07:37:49', '2022-10-09 07:37:49'),
('395022009c5813efa65e0b0725510d6cabcc5641a29ef6fb14f5d3051224f7773c2b2a959091097d', NULL, 5, 'users', '[]', 0, '2021-10-11 02:49:24', '2021-10-11 02:49:24', '2022-10-11 02:49:24'),
('39da442b404b3dc296681e843001039fc958336a735aa324c890e11a857f574f9b8d627e8a527a73', 1, 3, 'users', '[]', 0, '2021-10-08 08:34:32', '2021-10-08 08:34:32', '2022-10-08 08:34:32'),
('39f45968230327d96b12b4cf9b47ba5079da5e5b41b8b2b3a46b95c54b3edcf86ca63b5354330220', 1, 3, 'users', '[]', 0, '2021-10-08 17:04:04', '2021-10-08 17:04:04', '2022-10-08 17:04:04'),
('3aaf3e30a2d997372c5b938d2cd4d6a0f6856144d127ca27cbeecc0d40e07d34bbe7b222c5a7baf1', 1, 3, 'users', '[]', 0, '2021-10-08 17:08:18', '2021-10-08 17:08:18', '2022-10-08 17:08:18'),
('3bb24957669281c8df4f76954b9d65744f0f81700bdca32c0770803c132a9373fb150eff3e17d499', 1, 3, 'users', '[]', 0, '2021-10-08 17:10:03', '2021-10-08 17:10:03', '2022-10-08 17:10:03'),
('3ceb08cacdd1d80dc8a82fba121839e007058118cd9b9cb595628ce913ce966b19a5d82943c999d8', 1, 3, 'users', '[]', 0, '2021-10-08 17:39:26', '2021-10-08 17:39:26', '2022-10-08 17:39:26'),
('3f61fbcb578db54d8f63cae39baf64fd2dd5e357a19e9cfd7c480f3bf514dc2d8abcf2e71dd2490a', 1, 5, 'users', '[]', 0, '2021-10-09 01:39:48', '2021-10-09 01:39:48', '2022-10-09 01:39:48'),
('3f75f189c7a6a6010cf8ce6aae888570d5309a6f3ddb2c8e10edc8c64fbab8a958c55000847aee52', 1, 3, 'users', '[]', 0, '2021-10-08 08:34:21', '2021-10-08 08:34:21', '2022-10-08 08:34:21'),
('407ecc7bd2f3180fb619c3c63cfc45849c33e6e6968688cba626018819e3a923bc876d8b5e9e39bd', 1, 1, 'users', '[]', 0, '2021-10-08 07:46:07', '2021-10-08 07:46:07', '2022-10-08 07:46:07'),
('4159f2474d8b4db5fad3458b852925ef0f976fcf2a52a18ea80f9c66f3f22537384b77fcfae51df6', 1, 3, 'users', '[]', 0, '2021-10-08 17:21:37', '2021-10-08 17:21:37', '2022-10-08 17:21:37'),
('42383f3c03ddc7dd00249f87d8fe23b3eb52ac7b3e84c82bb26e1ed0bec3ef8b5cd21d21d790180c', 1, 3, 'users', '[]', 0, '2021-10-08 17:15:18', '2021-10-08 17:15:18', '2022-10-08 17:15:18'),
('425b36411ce2229d3f829c09b3509247b3d393a97faada41b8c6ad61dcea13179fcd11fe170d9d75', 1, 3, 'users', '[]', 0, '2021-10-08 08:28:56', '2021-10-08 08:28:56', '2022-10-08 08:28:56'),
('43f5c052f368dfccd37c87af02d440ab1fdb415d64d77ba3f02568207d2a6a88614847c282b61b98', 1, 3, 'users', '[]', 0, '2021-10-08 17:04:07', '2021-10-08 17:04:07', '2022-10-08 17:04:07'),
('44eb1922bbd55a4b6dd113c33d8e1a086fc1d9a90da4b4b749a8245919b7ce189900ca85efdaf06f', 1, 3, 'users', '[]', 0, '2021-10-08 08:26:35', '2021-10-08 08:26:35', '2022-10-08 08:26:35'),
('470eefaf01d1e1e2ef6642b160da7800a67a5c6a15d242cd7052ed5740e3e8c3a35c778b49c5061e', NULL, 5, 'users', '[]', 0, '2021-10-11 08:48:12', '2021-10-11 08:48:12', '2022-10-11 08:48:12'),
('4b55a403f791c9e70dc89b2a9ed46fa617f3c6275a2956622c87ff558994c1145c020a30306c48a7', 1, 3, 'users', '[]', 0, '2021-10-08 08:12:01', '2021-10-08 08:12:01', '2022-10-08 08:12:01'),
('4b740fe2e13ad7c0f8d379615e23746a844b5f74bedde9807b7ce86709b55d0a158929de83516d8a', NULL, 5, 'users', '[]', 0, '2021-10-09 07:37:06', '2021-10-09 07:37:06', '2022-10-09 07:37:06'),
('4b9906114fc707886d2d487de0e3f2d0977e3cfa9ade5cba76f605b69449b9464b760e9b85949c53', 1, 3, 'users', '[]', 0, '2021-10-08 16:36:29', '2021-10-08 16:36:29', '2022-10-08 16:36:29'),
('4ce40f1527b0d696dc425b7000e1ff72499f9e8346d50791fc7b095b00bbad8c2f463259a0b52724', 1, 5, 'users', '[]', 0, '2021-10-09 04:28:37', '2021-10-09 04:28:37', '2022-10-09 04:28:37'),
('4e95b177ddd6ae05a7cb729135d1251cfe30817bfb2c656165f07011727a26799001d84a4a72a3db', 1, 3, 'users', '[]', 0, '2021-10-08 08:34:09', '2021-10-08 08:34:09', '2022-10-08 08:34:09'),
('5024a5231859b6197719ef84f35314e3de9dc5b4b1d0005bfe169e0a39f07de1efade4e828a5c544', 1, 3, 'users', '[]', 0, '2021-10-08 08:25:54', '2021-10-08 08:25:54', '2022-10-08 08:25:54'),
('50913eb7388e3d6d8c27fec1fce1c5d4ffc54be9323e4f82562710bf46c1f4b1cf8d4a91b373b536', NULL, 5, 'users', '[]', 0, '2021-10-09 07:39:07', '2021-10-09 07:39:07', '2022-10-09 07:39:07'),
('52bd49baea30d265df18c825c1852dd6fbe4886b76ead8d606c9f9560e63d407697e96f6a8ac281f', 1, 3, 'users', '[]', 0, '2021-10-08 17:17:49', '2021-10-08 17:17:49', '2022-10-08 17:17:49'),
('53a1e762bd35dfeb63b26ceeaa2b7d187ed0620e70cab5aa2e3dc348ccad1d13af65dee0c4638d3b', NULL, 5, 'users', '[]', 0, '2021-10-11 07:36:57', '2021-10-11 07:36:57', '2022-10-11 07:36:57'),
('54a82c1184da5736047623002ca8208799eac4511335bdee8d1b4fdcfba7bfb229255f0343946711', 1, 5, 'users', '[]', 0, '2021-10-09 03:18:39', '2021-10-09 03:18:39', '2022-10-09 03:18:39'),
('56205044a5049ac11ff4f8e29dd156160afa1d0f7e8ba697e96ba7a2e08176ed1d062ef7100b7398', 1, 3, 'users', '[]', 0, '2021-10-08 08:22:41', '2021-10-08 08:22:41', '2022-10-08 08:22:41'),
('568c1e14307de1a70309bfbe8b89021c52ab9be5eb870826693b514b4124891d48b5c18001a1a4d5', 1, 3, 'users', '[]', 0, '2021-10-08 17:02:58', '2021-10-08 17:02:58', '2022-10-08 17:02:58'),
('5865897a79c4ca5f217f57b92b47250a09ba5ee68e85db40aea98d7a9dce1cbc801378a3fc2cae2c', 1, 3, 'users', '[]', 0, '2021-10-08 08:38:20', '2021-10-08 08:38:20', '2022-10-08 08:38:20'),
('58c86b065ba0fce9e28f51c7a3c563cc3045eb6a32dbc21a248b45fc9a6953e9eb1be40ce56c366a', 1, 3, 'users', '[]', 0, '2021-10-08 17:10:21', '2021-10-08 17:10:21', '2022-10-08 17:10:21'),
('5a5830980f32b4f91685ab9f2640724079d703c4d831c8bbbcf8123665fb3502189170b693924479', 1, 3, 'users', '[]', 0, '2021-10-08 08:29:37', '2021-10-08 08:29:37', '2022-10-08 08:29:37'),
('5b89fc1198c569386df4c2465ef821525c86a5f9f9b41be21eacc2d544c0befef93a727282a4c7c5', 1, 3, 'users', '[]', 0, '2021-10-08 08:16:37', '2021-10-08 08:16:37', '2022-10-08 08:16:37'),
('5bf170636d2e281565c0c9f97b4448ac8b245ff432cc28083e9b5b49fc87f2b277f3b66cb8bae1bf', 1, 3, 'users', '[]', 0, '2021-10-08 08:32:55', '2021-10-08 08:32:55', '2022-10-08 08:32:55'),
('5fc89194c87000f48eef740b32b31411136df488ad483e4a8285db6b0d8b750cce267d9a7f9115c6', 1, 3, 'users', '[]', 0, '2021-10-08 08:25:49', '2021-10-08 08:25:49', '2022-10-08 08:25:49'),
('6193686fc6bc5b446ea7be4a67db47675502bf07246ca0aaae67cf1725f1750a3553a0b32e31fb91', 1, 3, 'users', '[]', 0, '2021-10-08 17:15:34', '2021-10-08 17:15:34', '2022-10-08 17:15:34'),
('634960ddbab6f298a19f3168563aa944acbfab45ff28f07fd3f206c11529d6693f6e209d2f884e7f', 1, 3, 'users', '[]', 0, '2021-10-08 17:04:00', '2021-10-08 17:04:00', '2022-10-08 17:04:00'),
('640ecb59eea469f2e1e453d6733dd9f48696eaeedad624ec3b09d757f997291f4811827898777d6c', 1, 3, 'users', '[]', 0, '2021-10-08 16:58:31', '2021-10-08 16:58:31', '2022-10-08 16:58:31'),
('6423766537d470c79f207fd07d728694ec4115195856b7db1d376e28b987e530919463ed29121dcc', 1, 3, 'users', '[]', 0, '2021-10-08 08:57:42', '2021-10-08 08:57:42', '2022-10-08 08:57:42'),
('6512b4f00eb99a7272d5f3406135e4cbfc6105a76689bd085089e1453437e54652e5a5436f17e7d4', 1, 5, 'users', '[]', 0, '2021-10-09 04:34:03', '2021-10-09 04:34:03', '2022-10-09 04:34:03'),
('65cb1de9bd57ccb29cc51472c1665008738ab84f305b47c8c2fef79d855bffb9076c957775365824', 1, 3, 'users', '[]', 0, '2021-10-08 17:24:42', '2021-10-08 17:24:42', '2022-10-08 17:24:42'),
('66bcc8adda1cf99d694ae0c4aaec736a4dd0b2e6709c7af92a0af62593d49d3d767044451cf12262', 1, 5, 'users', '[]', 0, '2021-10-08 18:15:33', '2021-10-08 18:15:33', '2022-10-08 18:15:33'),
('67baa9be3b420f5eea4367c3ae4b39e4a4a70d62f9d417fcb1ebab4e41b589c05f1ed7bed173d366', 1, 3, 'users', '[]', 0, '2021-10-08 08:16:58', '2021-10-08 08:16:58', '2022-10-08 08:16:58'),
('6ebff5528e15fd0149bebe2231cd14e2aecd54d60f6186a9088bed5fd4ce3d0ded334415b163a203', NULL, 5, 'users', '[]', 0, '2021-10-11 07:34:04', '2021-10-11 07:34:04', '2022-10-11 07:34:04'),
('6fbbcb2a9b30914e5d615a6671a8aa5d26998be5aaf7919d367a233c14e38103d1405bf590289157', 1, 3, 'users', '[]', 0, '2021-10-08 08:26:40', '2021-10-08 08:26:40', '2022-10-08 08:26:40'),
('700c4bb04d504efd9e7665b271e010d985527d72ebe259c1cc1d8833be929c1a3c5092b520e1746d', 1, 5, 'users', '[]', 0, '2021-10-09 04:29:02', '2021-10-09 04:29:02', '2022-10-09 04:29:02'),
('720f31eed8ae621ff43a45e1ac8d3d1cb132b16829b44fd42865ef5020c148dd5051609cc69362be', 2, 5, 'users', '[]', 0, '2021-10-08 18:16:36', '2021-10-08 18:16:36', '2022-10-08 18:16:36'),
('72797c64035edcc03fe7b60fda52db77838ce94ea1731c0f66b1b831974c8b881b45b777d03a06e5', 1, 3, 'users', '[]', 0, '2021-10-08 08:34:34', '2021-10-08 08:34:34', '2022-10-08 08:34:34'),
('736f778ea1aabe466c5ebbee0568ac15dae09e49fe1a4e46d9a3ef2ef23a5ae2ba0e33a18e681fa4', 1, 5, 'users', '[]', 0, '2021-10-09 07:00:52', '2021-10-09 07:00:52', '2022-10-09 07:00:52'),
('74c55681a141474c8450dbb07c7fdcf4b99d487a01231ab44a1facae27e8f324aba41620dd4cdac3', 1, 5, 'users', '[]', 0, '2021-10-09 03:52:01', '2021-10-09 03:52:01', '2022-10-09 03:52:01'),
('753ec1d89720607d496abb6c962a8f328dc32e9753520a5dece86b8934f2b775698b4d9afeee94b0', 1, 3, 'users', '[]', 0, '2021-10-08 16:58:08', '2021-10-08 16:58:08', '2022-10-08 16:58:08'),
('760563a6ea6cc06e5b2fc4de8aa2493f1fd4d24241b74f47eb9445738574498b9b5eb2cba3b1217a', 1, 3, 'users', '[]', 0, '2021-10-08 17:45:11', '2021-10-08 17:45:11', '2022-10-08 17:45:11'),
('7921e1777c241694e70a25cc22dc6bd10b458066d64da319ab55c1324b536566e01743705b76a21e', 1, 3, 'users', '[]', 0, '2021-10-08 17:04:02', '2021-10-08 17:04:02', '2022-10-08 17:04:02'),
('7c7b76f0f6641b9745e8e33dc552b3afbc2b4d1d608bebd55abb1ea05f7bf8ffc433817371b0da72', 1, 3, 'users', '[]', 0, '2021-10-08 17:02:20', '2021-10-08 17:02:20', '2022-10-08 17:02:20'),
('7e031f15e8369bed835f096cc07997de3c45db35826ca0976409da5f897bfde0afab488345dc029b', 1, 3, 'users', '[]', 0, '2021-10-08 17:04:04', '2021-10-08 17:04:04', '2022-10-08 17:04:04'),
('7fa2b261b6b67921d90b758b6acdfb22468264149a68651ae0101ddc3be57e3443f7566284ba66d3', 1, 3, 'users', '[]', 0, '2021-10-08 17:02:49', '2021-10-08 17:02:49', '2022-10-08 17:02:49'),
('7fcaeea95bc2ae82f5db0242b88b688a8372ccc69fef07a43172c8cfeddfe287dd647b6c841011fd', 1, 3, 'users', '[]', 0, '2021-10-08 08:38:23', '2021-10-08 08:38:23', '2022-10-08 08:38:23'),
('8029adf8f35806ae41499c7ce953feadaba6b212ddcd02f17f51fdade5b734dcd4d50270617b69a1', 1, 3, 'users', '[]', 0, '2021-10-08 16:04:23', '2021-10-08 16:04:23', '2022-10-08 16:04:23'),
('80589720bbc316c61d60cf59ac1c81701d916ab106c17fa43dce1fa4bcd89dbaf6a1189f82e4a79e', NULL, 5, 'users', '[]', 0, '2021-10-12 03:06:45', '2021-10-12 03:06:45', '2022-10-12 03:06:45'),
('811d49ba8683766f2922d1f25c5bebc50ee152fb32b9813016af31a4e7b6a7bab465cf153aa7e7fa', 1, 1, 'users', '[]', 0, '2021-10-08 08:07:15', '2021-10-08 08:07:15', '2022-10-08 08:07:15'),
('824ab23caaff61e77944a9099cce0fcc969b1bda6909a656710a328349273e7e77e5dec7202dc609', 1, 3, 'users', '[]', 0, '2021-10-08 08:40:15', '2021-10-08 08:40:15', '2022-10-08 08:40:15'),
('82d8994a2a5c32c6ccc280b5a18ee9c63309e14aff5cd948578cb815fc40591a67e94accb573eaa1', 1, 3, 'users', '[]', 0, '2021-10-08 17:02:24', '2021-10-08 17:02:24', '2022-10-08 17:02:24'),
('833a10baad23e58a13676af3d5080832cb02a9e85212a8ab4196df8a3dc3bb3be4488356845b9e0b', 1, 3, 'users', '[]', 0, '2021-10-08 16:59:57', '2021-10-08 16:59:57', '2022-10-08 16:59:57'),
('843a4d291fe4f05b8e5343ebce32f4d2e5231aad45c1ef8ec306a0f95cf29b46791e32ab60f8fff6', 1, 5, 'users', '[]', 0, '2021-10-09 03:38:00', '2021-10-09 03:38:00', '2022-10-09 03:38:00'),
('8640b41450ece9d43e68b8bfa303c06cb3c73cb5d49a1867e1dd19d5505d480dfbf1b3d68bda5fd9', 1, 3, 'users', '[]', 0, '2021-10-08 08:23:23', '2021-10-08 08:23:23', '2022-10-08 08:23:23'),
('8961d3852e8c8082cb44f475cbf109fc3c0c138b468d58fc0e6c1fbc1e3a7a2486adcf67737a53c0', 1, 5, 'users', '[]', 0, '2021-10-09 01:49:02', '2021-10-09 01:49:02', '2022-10-09 01:49:02'),
('89bb3decfdd0b1bff5c37a8c70d3a8ab8f25003246a43e2a516f8ec230012234b06a5bf70250acf0', 1, 3, 'users', '[]', 0, '2021-10-08 08:57:44', '2021-10-08 08:57:44', '2022-10-08 08:57:44'),
('93c6efec446e8a2f0cfa02a64c0d71fa5f02560700b80673421ff0c6bb8f4b8439a3cf292c409c5a', 1, 3, 'users', '[]', 0, '2021-10-08 17:41:09', '2021-10-08 17:41:09', '2022-10-08 17:41:09'),
('947e23b29e128c3e393650b1ed109fa21117c0dca00ea3639f1d575c3cab2f338c342684c6b7df75', 1, 3, 'users', '[]', 0, '2021-10-08 17:02:23', '2021-10-08 17:02:23', '2022-10-08 17:02:23'),
('955b8f945e7c41fb30284889f5d5e8d65685045653a6d262d67e84f4af708a314a4e0458bded40d4', 1, 5, 'users', '[]', 0, '2021-10-09 03:52:06', '2021-10-09 03:52:06', '2022-10-09 03:52:06'),
('96563712897eea8451757ac1b3e4fd89131d99f039c1932a3ced4ec6cef9170867bd0e4a3370b36e', 1, 5, 'users', '[]', 0, '2021-10-09 04:05:22', '2021-10-09 04:05:22', '2022-10-09 04:05:22'),
('9b0613263f44ac14d4830e2e358bd970ff70cf29fdc17331eebc0f00dcd18ea994c688ed9902c035', NULL, 5, 'users', '[]', 0, '2021-10-11 08:12:53', '2021-10-11 08:12:53', '2022-10-11 08:12:53'),
('9b5340e7781f475eb8de0708348baaacfd635ab1720d6cae7d26c77fb123cb436d1342684cdc5c1b', 1, 3, 'users', '[]', 0, '2021-10-08 08:23:17', '2021-10-08 08:23:17', '2022-10-08 08:23:17'),
('a047a356ae56b7f27c0e2f682915a2b16666e8de29c39af6f4bb0d7ccd157c385b330827330b9e71', 1, 3, 'users', '[]', 0, '2021-10-08 08:57:41', '2021-10-08 08:57:41', '2022-10-08 08:57:41'),
('a2164b36840ac3aeb11e3e353b9919e5be87f291b3b10ccf3b8d9803057eee6ea7bcad7e2a1550d4', 1, 3, 'users', '[]', 0, '2021-10-08 17:02:21', '2021-10-08 17:02:21', '2022-10-08 17:02:21'),
('a2cef14d570a1ab5a418420bb873a30d6ed4c27ce82b02cbe4c2f0b68b01b28406eec270587ee33b', 1, 3, 'users', '[]', 0, '2021-10-08 17:24:22', '2021-10-08 17:24:22', '2022-10-08 17:24:22'),
('a3743069a110be52ac69246fba0a5a57f3228b995f279273401e41465ecef4e4a74b151739f594e3', 1, 1, 'users', '[]', 0, '2021-10-08 07:41:52', '2021-10-08 07:41:52', '2022-10-08 07:41:52'),
('a3be61684b06f0fa9f9cf9b58009a93bc1fc0ee67640e4e8162e92950631d89b4f962e23746e90e5', 1, 1, 'users', '[]', 0, '2021-10-08 07:48:33', '2021-10-08 07:48:33', '2022-10-08 07:48:33'),
('a604a3d95421fe8df62fa465e708795bfa4cdf0fc5d74eb78141d6b6d6e25c57f39327ac8a26a009', NULL, 5, 'users', '[]', 0, '2021-10-11 09:13:59', '2021-10-11 09:13:59', '2022-10-11 09:13:59'),
('a6d1140869559b1be01a8bafd582027077cc7a50a0e41279ab3515a0debe0580e86bb92f4e6d2ab7', 1, 3, 'users', '[]', 0, '2021-10-08 08:29:17', '2021-10-08 08:29:17', '2022-10-08 08:29:17'),
('aa640f4aed81ecb903930ad753342fd4ec5e6a62c0e4d5507f7961e31df08fc369cd8b13cca80d4b', 1, 3, 'users', '[]', 0, '2021-10-08 08:16:47', '2021-10-08 08:16:47', '2022-10-08 08:16:47'),
('af4fb59206c4268bb5aeab739637f6a498ff8182c41444b17cafc5187da85612b7aceffcbb6d3a1e', 1, 3, 'users', '[]', 0, '2021-10-08 08:16:55', '2021-10-08 08:16:55', '2022-10-08 08:16:55'),
('b06e2e46808474667e20ec1585e86d746fc2dde1eef20512e238df4c911d4464f37054662c2be0b5', 1, 3, 'users', '[]', 0, '2021-10-08 16:06:46', '2021-10-08 16:06:46', '2022-10-08 16:06:46'),
('b1861142299032380b69eced284afb7fab6ef367541bd12731a37c87aa5a6e52815cd46f0d46a8b3', 1, 3, 'users', '[]', 0, '2021-10-08 08:16:51', '2021-10-08 08:16:51', '2022-10-08 08:16:51'),
('b2923d99d24782e1dd071ff7e4c060d703189ce0bdf66ea98ad2cfc4c3436ca67c8c7dcc97f04ac1', 1, 3, 'users', '[]', 0, '2021-10-08 16:56:44', '2021-10-08 16:56:44', '2022-10-08 16:56:44'),
('b377fc71ddb3a8d4cc6e51067b4c7b7e29fe58ccb282fdf3621dc71d9b27a94933ad60e4b3c9c3a8', 1, 3, 'users', '[]', 0, '2021-10-08 08:57:39', '2021-10-08 08:57:39', '2022-10-08 08:57:39'),
('b544f89afcfa7ef399a999661fd34153503351bdd15db83eb8e948be9973367926241a64bb99a863', 1, 3, 'users', '[]', 0, '2021-10-08 08:23:22', '2021-10-08 08:23:22', '2022-10-08 08:23:22'),
('b5c024cf35022d195af2ba8439985531d0a4458262261df162f82f2bba7f7f9b2feb5a5fe2e6ca8a', 1, 3, 'users', '[]', 0, '2021-10-08 17:15:50', '2021-10-08 17:15:50', '2022-10-08 17:15:50'),
('b75565253025c3ff225da206c0a2407a760ef4719345d80aa377abc2d0bff9b62a36693b5e4eb885', NULL, 5, 'users', '[]', 0, '2021-10-11 09:14:08', '2021-10-11 09:14:08', '2022-10-11 09:14:08'),
('b9db4a5d2a32fac8a5acdd2574a48a07ee0a94581bd61fef979ea708bdc6940f79c2bf494ec04874', 1, 3, 'users', '[]', 0, '2021-10-08 17:03:20', '2021-10-08 17:03:20', '2022-10-08 17:03:20'),
('ba46bed83b4934efa97940fb10a5b39278ea28b22fd03df78fd17425d95091dcb5eb678800268b99', 1, 3, 'users', '[]', 0, '2021-10-08 08:16:57', '2021-10-08 08:16:57', '2022-10-08 08:16:57'),
('bd63aed77b6d939669b038c6c22ee05a625b845cf60ec758d69ee5e72392020b52e0dcd2b8a953f7', 1, 3, 'users', '[]', 0, '2021-10-08 17:03:05', '2021-10-08 17:03:05', '2022-10-08 17:03:05'),
('be0d78f51fb9ef47e89ea1204682f116ad344dc3baa38053aa0ddd51ff1a5c375eba95935296aae3', 1, 3, 'users', '[]', 0, '2021-10-08 08:38:24', '2021-10-08 08:38:24', '2022-10-08 08:38:24'),
('be18b9e708239c941aa9e60350bac64fe7f3a8e576b8be66e9f302e7b626bd54c55fb16b5f9d4043', 1, 3, 'users', '[]', 0, '2021-10-08 08:16:43', '2021-10-08 08:16:43', '2022-10-08 08:16:43'),
('c28512e932449c4c0cdc28725acb3c4248fa4e63cd963994c6f0e60f18dd35df147cdbfa37185976', 1, 5, 'users', '[]', 0, '2021-10-09 03:52:40', '2021-10-09 03:52:40', '2022-10-09 03:52:40'),
('c59a52df7ec2a5eab6c3321e2643e85b0cd75b7811acec4429583921edc1057fa8b6d5fba3bbdbd6', 1, 5, 'users', '[]', 0, '2021-10-09 06:54:14', '2021-10-09 06:54:14', '2022-10-09 06:54:14'),
('c9e95ac85dad1d3c05cba4ab055272f791d27e6a51690d684dce626e5d6e2f0f5c40383408684336', 1, 1, 'users', '[]', 0, '2021-10-08 07:42:02', '2021-10-08 07:42:02', '2022-10-08 07:42:02'),
('cb277d4a13530549ffd305e4da7fcc36210106fed0fb1a9b2edcdb80a87c234c646364cec1e3f605', 1, 3, 'users', '[]', 0, '2021-10-08 17:15:25', '2021-10-08 17:15:25', '2022-10-08 17:15:25'),
('cbc5bd46297d0c35b8ffe3733c5b851935244738f8eaef3963ec29db09c020fbcbe9e89c54c9843a', NULL, 5, 'users', '[]', 0, '2021-10-11 07:38:34', '2021-10-11 07:38:34', '2022-10-11 07:38:34'),
('cbfdf731dfc4a107f19081358ef5d890b5fd614359909521ad87bbb92c2696df810f8822bf83707d', 1, 3, 'users', '[]', 0, '2021-10-08 17:07:16', '2021-10-08 17:07:16', '2022-10-08 17:07:16'),
('ce0bf40ef2e20f0f9cb0b2c3e4c674a6eb3a3d3cf6cf672576e4281fe313a59547c9c1a388768dc2', 1, 3, 'users', '[]', 0, '2021-10-08 08:40:22', '2021-10-08 08:40:22', '2022-10-08 08:40:22'),
('d256b02656b20ccdc14548a4ee65d57dbc2879d052aa211a4ade15b6acb0f64eb335fa82de19bb47', 1, 5, 'users', '[]', 0, '2021-10-09 04:06:32', '2021-10-09 04:06:32', '2022-10-09 04:06:32'),
('d3dbbddaa4e8010cbc5f31eeb65a06eab873ad49b0572d1793bb467b4f4e271382b44f7546f56c4b', 1, 3, 'users', '[]', 0, '2021-10-08 16:58:11', '2021-10-08 16:58:11', '2022-10-08 16:58:11'),
('d4eaa0404b4f939315cbb09ac534ebeae20189e0e93a62c679f1e6394b08bd9c8085d10edb33a57b', 1, 3, 'users', '[]', 0, '2021-10-08 08:25:47', '2021-10-08 08:25:47', '2022-10-08 08:25:47'),
('d55857597a3258f77527119b0954ef787ee586c960e34424c6129f10e19ed15d203205be5878858f', 1, 3, 'users', '[]', 0, '2021-10-08 16:36:37', '2021-10-08 16:36:37', '2022-10-08 16:36:37'),
('d80773e2e54334ead7390e77056572f8aa94066e6da8f78c879eb43cabef1daa0d51933ba7eeb1c8', 1, 5, 'users', '[]', 0, '2021-10-09 03:53:16', '2021-10-09 03:53:16', '2022-10-09 03:53:16'),
('d9017ddb5dc7a1022e2fc143f810e220ce7bba9e5fa925652c8d72130d71dc652bc4580fc91abcdd', NULL, 5, 'users', '[]', 0, '2021-10-11 07:34:23', '2021-10-11 07:34:23', '2022-10-11 07:34:23'),
('d953e71a8c70b2f675c128845682df838e9d36197b24541503b769e9257d8c6b77b977e0d6442bad', 1, 3, 'users', '[]', 0, '2021-10-08 16:06:44', '2021-10-08 16:06:44', '2022-10-08 16:06:44'),
('dc5aa287c4226062279a4352b24e4c98eb000404cb62b355fe9657fab0334f6c4ac724c9c8cd3426', NULL, 5, 'users', '[]', 0, '2021-10-09 07:02:24', '2021-10-09 07:02:24', '2022-10-09 07:02:24'),
('df9a0b953a9d525a44099684d70fc511f618ee4c3d0701a81c709b95e6a5ef94fe6a9be6392729e0', 1, 5, 'users', '[]', 0, '2021-10-09 03:18:09', '2021-10-09 03:18:09', '2022-10-09 03:18:09'),
('e1187dda6a664c4c8ac170493c919ff49071235dcef5757dd6e24d2d1229e101fe884bb34d052539', 1, 5, 'users', '[]', 0, '2021-10-09 03:17:41', '2021-10-09 03:17:41', '2022-10-09 03:17:41'),
('e2762f332b78f0918d37a9c88300f71dab7669cfceb0aedb7b2ec0a57752c4671e67edfc7ec88f64', 1, 3, 'users', '[]', 0, '2021-10-08 17:58:01', '2021-10-08 17:58:01', '2022-10-08 17:58:01'),
('e3c57cbebfeacf10d6e206d350b9ceed7df62cfa876616f8b6a6d318d8ce92d6c2f8e02c996f2a77', 1, 3, 'users', '[]', 0, '2021-10-08 17:04:06', '2021-10-08 17:04:06', '2022-10-08 17:04:06'),
('e8258291b87d667c3f4187dce298b45bea8172b14a092bb122be765904bdb222c787959d80908f4c', 1, 3, 'users', '[]', 0, '2021-10-08 09:12:49', '2021-10-08 09:12:49', '2022-10-08 09:12:49'),
('f0dfe35c4e27af2e35bb53897ee9e73c407b2cde76efaf9a4e4e4111e5ff4e365648d7320cbeb1af', NULL, 5, 'users', '[]', 0, '2021-10-11 04:26:14', '2021-10-11 04:26:14', '2022-10-11 04:26:14'),
('f3e17a94324dbe272e7da3d0e228381c9cdf5d2c9cd06aaf4bf45b3ebf9ed0407081c3a35b8cd03b', NULL, 5, 'users', '[]', 0, '2021-10-11 07:42:08', '2021-10-11 07:42:08', '2022-10-11 07:42:08'),
('f4f72cc4291cbada6154e444fbe5b2f5be41391371a8064f53f73664a7e4dbe4fd26bcdee59837b7', 1, 3, 'users', '[]', 0, '2021-10-08 17:20:44', '2021-10-08 17:20:44', '2022-10-08 17:20:44'),
('f870002bb50c611c39907aa0ad87573058fd03d2edbcda4021d5f4619b25f4df4ecfa4745ed1d038', NULL, 5, 'users', '[]', 0, '2021-10-11 07:36:17', '2021-10-11 07:36:17', '2022-10-11 07:36:17'),
('f8dea66022320d14847e920b7f1c15cdfb7705bb2c9278ef81fa2f2950f959a76976944e0384630b', 1, 3, 'users', '[]', 0, '2021-10-08 17:39:12', '2021-10-08 17:39:12', '2022-10-08 17:39:12'),
('f9063180167c747e2b88fbccd7206a36db275c6517b1fe777ca170411be56c9cd743ccfa145df4c9', 1, 3, 'users', '[]', 0, '2021-10-08 08:40:46', '2021-10-08 08:40:46', '2022-10-08 08:40:46'),
('fba4464a7ee5651d186fc6d298be08438bc51027fdc77fa107e55538c04bac345d1711c19731d33d', 1, 3, 'users', '[]', 0, '2021-10-08 17:38:50', '2021-10-08 17:38:50', '2022-10-08 17:38:50'),
('fec59ef43f5f95f0a5c7b36e757af2f863c8d8e181dc102202f7dfd0af508829606c09697270dc2c', 1, 3, 'users', '[]', 0, '2021-10-08 17:02:17', '2021-10-08 17:02:17', '2022-10-08 17:02:17');

-- --------------------------------------------------------

--
-- Table structure for table `oauth_auth_codes`
--

CREATE TABLE `oauth_auth_codes` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `client_id` bigint(20) UNSIGNED NOT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `oauth_clients`
--

CREATE TABLE `oauth_clients` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `secret` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `provider` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `redirect` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `personal_access_client` tinyint(1) NOT NULL,
  `password_client` tinyint(1) NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `oauth_clients`
--

INSERT INTO `oauth_clients` (`id`, `user_id`, `name`, `secret`, `provider`, `redirect`, `personal_access_client`, `password_client`, `revoked`, `created_at`, `updated_at`) VALUES
(1, NULL, 'Lumen Personal Access Client', 'J9OgQrlS2fOLwY0kaqkMBzQFfhjN93IcOJeSHk0E', NULL, 'http://localhost', 1, 0, 0, '2021-10-08 01:38:09', '2021-10-08 01:38:09'),
(2, NULL, 'Lumen Password Grant Client', 'AAA2tEEkfdG16lQu8DiUcrdc71e7UbZsMNNpC559', 'users', 'http://localhost', 0, 1, 0, '2021-10-08 01:38:09', '2021-10-08 01:38:09'),
(3, NULL, 'Lumen Personal Access Client', '6XahJuDSn6QrPAof4Koxwir9QN35bj1iWfI7JVyj', NULL, 'http://localhost', 1, 0, 0, '2021-10-08 08:08:02', '2021-10-08 08:08:02'),
(4, NULL, 'Lumen Password Grant Client', 'MpXMOZCq56ZIqiZHTszEvGnAAkgSn6KFoNqNf8R5', 'users', 'http://localhost', 0, 1, 0, '2021-10-08 08:08:02', '2021-10-08 08:08:02'),
(5, NULL, 'Lumen Personal Access Client', 'U2DAOhjlP0lPVZWd3mZsPUKTF65Ys2m87Cq8cE82', NULL, 'http://localhost', 1, 0, 0, '2021-10-08 18:09:31', '2021-10-08 18:09:31'),
(6, NULL, 'Lumen Password Grant Client', 'TvyAWrULkyUSFJw6zSEkr9SwTDAlUuWWhjvDW9ZT', 'users', 'http://localhost', 0, 1, 0, '2021-10-08 18:09:31', '2021-10-08 18:09:31');

-- --------------------------------------------------------

--
-- Table structure for table `oauth_personal_access_clients`
--

CREATE TABLE `oauth_personal_access_clients` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `client_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `oauth_personal_access_clients`
--

INSERT INTO `oauth_personal_access_clients` (`id`, `client_id`, `created_at`, `updated_at`) VALUES
(1, 1, '2021-10-08 01:38:09', '2021-10-08 01:38:09'),
(2, 3, '2021-10-08 08:08:02', '2021-10-08 08:08:02'),
(3, 5, '2021-10-08 18:09:31', '2021-10-08 18:09:31');

-- --------------------------------------------------------

--
-- Table structure for table `oauth_refresh_tokens`
--

CREATE TABLE `oauth_refresh_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `access_token_id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `uuid` varchar(32) COLLATE utf8mb4_unicode_ci NOT NULL,
  `displayName` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `about` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `userRole` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(60) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`uuid`, `displayName`, `about`, `status`, `userRole`, `email`, `password`, `created_at`, `updated_at`) VALUES
('3dc3f88a47524e1a82419cee43db0a67', 'Joesph Kling II', 'Fugiat vel sunt velit ea nisi. Commodi aliquid sequi et aliquid. Id quidem corporis rerum itaque quos modi velit. Sed eum sapiente dignissimos vel ipsum ut.', 'online', 'admin', 'quentin56@example.net', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', '2021-10-09 07:36:33', '2021-10-09 07:36:33'),
('406cb673e0e0449093575d441168709a', 'Dion Blick', 'Repudiandae velit voluptatum consequatur nemo animi ut. Reiciendis officia enim nobis ipsum. Dolor qui iste sequi praesentium est voluptas.', 'online', 'admin', 'streich.tessie@example.org', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', '2021-10-11 04:16:22', '2021-10-11 04:16:22'),
('458bb37d4bb84e29aad32baf43d71874', 'Bernita Lehner DDS', 'Repudiandae facilis quia fuga quod praesentium impedit. Aperiam voluptas cum quisquam ut. Id veritatis consectetur voluptatum dignissimos aut libero quas. Voluptates vitae recusandae vel rerum. Et quia corrupti repellat.', 'online', 'admin', 'abdiel81@example.com', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', '2021-10-11 04:16:22', '2021-10-11 04:16:22'),
('4750f6c355a04049a56c89ed2313acb1', 'Anna Bartoletti', 'Porro tempore odio quas. Nam laboriosam culpa aperiam aut aut.', 'online', 'admin', 'jgorczany@example.com', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', '2021-10-11 04:16:22', '2021-10-11 04:16:22'),
('4cc02073d96a4fbe8191356b04ede20c', 'Miller Wilderman', 'Saepe dolor aperiam inventore ipsum. Ducimus quasi et assumenda voluptas et maiores.', 'online', 'admin', 'amani.ankunding@example.org', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', '2021-10-09 07:36:33', '2021-10-09 07:36:33'),
('79aad3ff43df4b33af9e810e3c80db64', 'Lauren Stamm', 'Dolorum illo doloremque ipsam quae. Suscipit omnis architecto et voluptatem. Accusantium qui aut ut dolor sequi et. Nostrum nam dolorum animi minus in nam.', 'online', 'admin', 'eschamberger@example.org', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', '2021-10-09 07:36:33', '2021-10-09 07:36:33'),
('970aa9e247de4f77a825119c0d4ff1e6', 'Prof. Frederic Lowe Sr.', 'Explicabo dolorum et et porro eaque consequatur. Molestiae commodi blanditiis enim incidunt magnam rem. Sed deleniti quisquam consequatur mollitia nisi nam iste.', 'online', 'admin', 'nwiza@example.com', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', '2021-10-11 04:16:22', '2021-10-11 04:16:22'),
('9cff0f06177a49e7a45880eb08e2f0d0', 'Nolan Walker', 'Nihil sequi aspernatur minus in cumque rerum. Numquam rerum possimus assumenda minima in ut.', 'online', 'admin', 'mhickle@example.com', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', '2021-10-11 04:16:22', '2021-10-11 04:16:22'),
('c9f4fe65183542a1a0f6b276de92eb64', 'Vincenza Wyman', 'Ipsum explicabo quam distinctio vero aut. Sunt laborum omnis cumque maiores molestias temporibus voluptas. Dolor impedit voluptatem omnis praesentium repellat eos debitis. Quos nisi dolorum voluptatum nostrum provident illo.', 'online', 'admin', 'hansen.meggie@example.net', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', '2021-10-09 07:36:33', '2021-10-09 07:36:33'),
('dd1cdc20b3e34b98b5e714a30a617efa', 'Marcella Hills', 'Ex error dolorum et. Ab illo molestiae et eos dolorem aut. Ea minima iure dolorem perspiciatis voluptate consectetur ut.', 'online', 'admin', 'cruz08@example.org', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', '2021-10-09 07:36:33', '2021-10-09 07:36:33');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `authors`
--
ALTER TABLE `authors`
  ADD PRIMARY KEY (`uuid`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oauth_access_tokens`
--
ALTER TABLE `oauth_access_tokens`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_access_tokens_user_id_index` (`user_id`);

--
-- Indexes for table `oauth_auth_codes`
--
ALTER TABLE `oauth_auth_codes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_auth_codes_user_id_index` (`user_id`);

--
-- Indexes for table `oauth_clients`
--
ALTER TABLE `oauth_clients`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_clients_user_id_index` (`user_id`);

--
-- Indexes for table `oauth_personal_access_clients`
--
ALTER TABLE `oauth_personal_access_clients`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oauth_refresh_tokens`
--
ALTER TABLE `oauth_refresh_tokens`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_refresh_tokens_access_token_id_index` (`access_token_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`uuid`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `oauth_clients`
--
ALTER TABLE `oauth_clients`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `oauth_personal_access_clients`
--
ALTER TABLE `oauth_personal_access_clients`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
