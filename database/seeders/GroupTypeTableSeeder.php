<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class GroupTypeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('group_type')->delete();
        
        \DB::table('group_type')->insert(array (
            0 => 
            array (
                'id' => '2581506d4699417a8ee9517217b8cc5a',
                'name' => 'Administrator',
                'slug' => 'administrator',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            1 => 
            array (
                'id' => '2581506d4699417a8ee9517217b8cc5b',
                'name' => 'SPV Operasional',
                'slug' => '',
                'created_at' => '2019-01-10 11:22:29',
                'updated_at' => '2019-01-10 11:22:29',
            ),
            2 => 
            array (
                'id' => '2581506d4699417a8ee9517217b8cc5c',
                'name' => 'Senior Operasional Staff',
                'slug' => '',
                'created_at' => '2019-01-10 11:22:50',
                'updated_at' => '2019-01-10 11:22:50',
            ),
            3 => 
            array (
                'id' => '2581506d4699417a8ee9517217b8cc5d',
                'name' => 'Operasional Staff',
                'slug' => '',
                'created_at' => '2019-01-10 11:23:01',
                'updated_at' => '2019-01-10 11:23:01',
            ),
            4 => 
            array (
                'id' => '2581506d4699417a8ee9517217b8cc5e',
                'name' => 'Finance Staff',
                'slug' => '',
                'created_at' => '2019-01-10 11:23:41',
                'updated_at' => '2019-01-10 11:23:41',
            ),
            5 => 
            array (
                'id' => '2581506d4699417a8ee9517217b8cc5f',
                'name' => 'Komisaris',
                'slug' => '',
                'created_at' => '2019-01-14 13:24:58',
                'updated_at' => '2019-01-14 13:24:58',
            ),
            6 => 
            array (
                'id' => '2581506d4699417a8ee9517217b8cc5g',
                'name' => 'Owner',
                'slug' => '',
                'created_at' => '2019-02-26 16:00:40',
                'updated_at' => '2019-02-26 16:00:40',
            ),
            7 => 
            array (
                'id' => '2581506d4699417a8ee9517217b8cc5h',
                'name' => 'Manager',
                'slug' => '',
                'created_at' => '2019-09-13 15:40:24',
                'updated_at' => '2019-09-13 15:40:24',
            ),
            8 => 
            array (
                'id' => '2581506d4699417a8ee9517217b8cc5i',
                'name' => 'Admin',
                'slug' => '',
                'created_at' => '2019-09-13 15:40:41',
                'updated_at' => '2019-09-13 15:40:41',
            ),
            9 => 
            array (
                'id' => '2581506d4699417a8ee9517217b8cc5j',
                'name' => 'Gudang',
                'slug' => '',
                'created_at' => '2019-09-13 15:41:24',
                'updated_at' => '2019-09-13 15:41:24',
            ),
            10 => 
            array (
                'id' => '2581506d4699417a8ee9517217b8cc5k',
                'name' => 'Admin & Keuangan',
                'slug' => '',
                'created_at' => '2019-09-13 15:41:58',
                'updated_at' => '2019-09-13 15:41:58',
            ),
            11 => 
            array (
                'id' => '2581506d4699417a8ee9517217b8cc5l',
                'name' => 'Manajer Dislog',
                'slug' => '',
                'created_at' => '2021-07-23 10:07:37',
                'updated_at' => '2021-07-23 10:07:37',
            ),
            12 => 
            array (
                'id' => '2581506d4699417a8ee9517217b8cc5m',
                'name' => 'Manajer Umum',
                'slug' => '',
                'created_at' => '2021-08-04 17:18:57',
                'updated_at' => '2021-08-04 17:18:57',
            ),
            13 => 
            array (
                'id' => '2581506d4699417a8ee9517217b8cc5n',
                'name' => 'Supervisor',
                'slug' => '',
                'created_at' => '2018-12-28 11:16:55',
                'updated_at' => '2018-12-28 11:16:55',
            ),
            14 => 
            array (
                'id' => '2581506d4699417a8ee9517217b8cc5o',
                'name' => 'Marketing',
                'slug' => '',
                'created_at' => '2019-01-10 11:13:13',
                'updated_at' => '2019-01-10 11:13:13',
            ),
            15 => 
            array (
                'id' => '2581506d4699417a8ee9517217b8cc5p',
                'name' => 'Staff Akunting',
                'slug' => '',
                'created_at' => '2019-01-10 11:13:50',
                'updated_at' => '2019-01-10 11:13:50',
            ),
            16 => 
            array (
                'id' => '2581506d4699417a8ee9517217b8cc5q',
                'name' => 'Kasir Cabang',
                'slug' => '',
                'created_at' => '2019-01-10 11:14:07',
                'updated_at' => '2019-01-10 11:14:07',
            ),
            17 => 
            array (
                'id' => '2581506d4699417a8ee9517217b8cc5r',
                'name' => 'Direktur Utama',
                'slug' => '',
                'created_at' => '2019-01-10 11:21:18',
                'updated_at' => '2019-01-10 11:21:18',
            ),
            18 => 
            array (
                'id' => '2581506d4699417a8ee9517217b8cc5s',
                'name' => 'Direktur',
                'slug' => '',
                'created_at' => '2019-01-10 11:21:30',
                'updated_at' => '2019-01-10 11:21:30',
            ),
        ));
    }
}
