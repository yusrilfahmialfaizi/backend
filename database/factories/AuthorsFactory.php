<?php

namespace Database\Factories;

use App\Models\Authors;
use Ramsey\Uuid\Uuid;
use Illuminate\Database\Eloquent\Factories\Factory;

class AuthorsFactory extends Factory
{
    protected $model = Authors::class;

    public function definition(): array
    {
    	return [
            'uuid'                      => Uuid::uuid4()->getHex(),
            'name'                      => $this->faker->name,
            'email'                     => $this->faker->unique()->safeEmail,
            'github'                    => $this->faker->unique()->safeEmail,
            'twitter'                   => $this->faker->unique()->safeEmail,
            'latest_article_published'  => $this->faker->title,
            'location'                  => 'Surabaya',
        ];
    }
}
