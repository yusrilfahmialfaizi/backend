<?php

/** @var \Laravel\Lumen\Routing\Router $router */

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

// \Dusterio\LumenPassport\LumenPassport::routes($app, ['prefix' => 'v1/oauth']);

$router->get('/', function () use ($router) {
    return $router->app->version();
});


$router->group(['prefix' => 'api'], function () use ($router) {

  // Auth
  $router->post('register', ['uses'  => 'AuthController@register']);
  $router->post('login', ['uses'  => 'AuthController@login']);
  $router->get('activation/{id}', ['uses'  => 'AuthController@activate']);
  $router->post('forgot-password', ['uses'  => 'AuthController@forgotPassword']);
  $router->post('reset-password', ['uses'  => 'AuthController@resetPassword']);

  // Authors
  $router->get('authors', ['uses' => 'AuthorsController@showAllAuthors']);
  $router->get('kirim-wa', ['uses' => 'AuthorsController@kirimWa']);
  $router->get('authors/{id}', ['uses' => 'AuthorsController@showOneAuthor']);

  $router->post('authors', ['uses' => 'AuthorsController@create']);

  $router->delete('authors/{id}', ['uses' => 'AuthorsController@delete']);

  $router->put('authors/{id}', ['uses' => 'AuthorsController@update']);

  // Users
  $router->get('users', ['uses' => 'UsersController@showAllUsers']);
  $router->get('users/create', ['uses' => 'UsersController@getDataSelect']);
  $router->get('users/{id}', ['uses' => 'UsersController@showOneUser']);
  $router->post('users', ['uses' => 'UsersController@create']);
  $router->delete('users/{id}', ['uses' => 'UsersController@delete']);
  $router->put('users/{id}', ['uses' => 'UsersController@update']);
  
  $router->put('fcm_token/{id}', ['uses' => 'UsersController@update_fcmToken']);

  //Group
  $router->get('group', ['uses' => 'GroupController@showAllGroup']);

  $router->post('group', ['uses' => 'GroupController@create']);

  $router->put('group', ['uses' => 'GroupController@update']);

  $router->delete('group/{id}', ['uses' => 'GroupController@delete']);

  //Group Previlage
  $router->get('user/group-previlage/{id}', ['uses' => 'GroupController@showPrevilage']);

  $router->post('user/group-previlage', ['uses' => 'GroupController@createPermission']);
  
  //PUSH NOFIF FCM PWA
  $router->post('push-notif', ['uses' => 'UsersController@pushNotif']);
  
  //kirimWa.id
  $router->post('kirim-pesan', ['uses' => 'KirimWaController@kirimPesan']);

  $router->post('kirim-gambar', ['uses' => 'KirimWaController@kirimGambar']);

  $router->post('kirim-document', ['uses' => 'KirimWaController@kirimDocument']);
  
});
