<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use App\Models\User;
use Ramsey\Uuid\Uuid;
use App\Mail\TestMail;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Hash;

class AuthController extends Controller
{
    //
    public function __construct()
    {
        // $this->middleware('auth:api');
    }

    public function register(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => 'required',
            'password'  => 'required'
        ]);


        if (!$validator->fails()) {
            $user           = new User;
            $user->uuid     = Uuid::uuid4()->getHex();
            $user->name     = $request->name;
            $user->email    = $request->email;
            $user->password = Hash::make($request->password);
            // $user->save();
            if ($user->save()) {
                # code...
                $details    = [
                    'title'     => 'Hi '.$user->name.',',
                    'header'    => 'Your user account with the e-mail address '.$user->email.' has been created.',
                    'body'      => 'Please follow the link below to activate your account.',
                    'link'      => 'http://backend.local/api/activation/'.$user->uuid,
                    'footer'    => 'You will be able to change your settings (password, language, etc.) once your account is activated.'
                ];
                Mail::to($request->email)->send(new TestMail($details));
                return response()->json([
                    'status'    => 200,
                    'message'   => 'Success',
                ], 201);
            }   
            return response()->json([
                'status'    => 400,
                'message'   => 'Bad Request',
            ], 401);
        }
    }

    public function login(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => 'required',
            'password'  => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json([
                'status'    => 500,
                'message'   => 'Bad Request',
                'error'     => $validator->errors(),
            ], 401);
        }
        if ($token = auth()->guard('web')->attempt($request->only('email', 'password'))) {
            if (auth()->guard('web')->user()['is_email_activate'] > 0) {
                # code...
                $response = [
                    'data'    => [
                        'accessToken'       => auth()->guard('web')->user()->createToken('users')->accessToken,
                        'userData'          => auth()->guard('web')->user(),
                        'is_email_activate' => auth()->guard('web')->user()['is_email_activate']
                        ]
                    ];
                return response()->json(['user' => $response]);
            }
            $response = [
                'data'    => [
                        'uuid'      => auth()->guard('web')->user()['uuid'],
                        'error'     => 401, 
                        'message'   => 'Email not activate. Please check your email.'
                    ]
                ];
                $details    = [
                    'title'     => 'Hi '.auth()->guard('web')->user()['name'].',',
                    'header'    => 'Your user account with the e-mail address '.auth()->guard('web')->user()['email'].' has been created.',
                    'body'      => 'Please follow the link below to activate your account.',
                    'link'      => 'http://backend.local/api/activation/'.auth()->guard('web')->user()['uuid'],
                    'footer'    => 'You will be able to change your settings (password, language, etc.) once your account is activated.'
                ];
            Mail::to($request->email)->send(new TestMail($details));
            return response()->json(['user' => $response]);
        }
        $response = [
            'data'    => [
                'error'=> 401, 'message' => 'Wrong Email or Password'
                ]
            ];
        return response()->json(['user' => $response]);
    }

    public function activate($id, Request $request)
    {
        $activate = User::findOrFail($id);
        $activate->is_email_activate = 1;
        $activate->save();
        return redirect('http://localhost:8080/login');
    }

    public function forgotPassword(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => 'required'
        ]);


        if (!$validator->fails()) {
            $email      = $request->email;
            $data       = User::where('email', $email)->first();

            $details    = [
                'title'     => 'Hi '.$data['name'].',',
                'header'    => 'We heard you need a password reset.',
                'body'      => "Click the link below and you'll be redirected to a secure site from which you can set a new password.",
                'link'      => 'http://localhost:8080/reset-password?uid='.$data['uuid'],
                'footer'    => 'You will be able to reset your password.'
            ];
            Mail::to($request->email)->send(new TestMail($details));
            return response()->json([
                'status'    => 200,
                'message'   => 'Success',
            ], 201);
        }
    }

    public function resetPassword(Request $request)
    {
        $user           = User::findOrFail($request->uid);
        $user->password = Hash::make($request->password);
        $user->save();
        return response()->json([
            'status'    => 200,
            'message'   => 'Success',
        ], 201);
    }

    public function isAuthorized(Request $request)
    {
    
        $user = auth()->guard('web')->user();
        if (!$user) {
            return response()->json([
            'isAuthorized' => false
            ], 401);
        }

        return response()->json([
            'isAuthorized' => true
        ], 200);
    }
}
