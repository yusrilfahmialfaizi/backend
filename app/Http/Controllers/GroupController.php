<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Ramsey\Uuid\Uuid;
use App\Models\Group;
use App\Models\Roles;
use App\Models\GroupRoles;
use Carbon\Carbon;

class GroupController extends Controller
{
    /**
     * Untuk mengambil data group
     */

    public function showAllGroup()
    {
        return response()->json(Group::all());
    }

    /**
     * Untuk insert data group
     * @param $request
     */

    public function create(Request $request)
    {
        $group = new Group;
        $group->id = Uuid::uuid4()->getHex();
        $group->name = $request->name;
        $group->save();

        return response($group, 201);
    }

    /**
     * Untuk update data group
     * @param $request
     */

    public function update(Request $request)
    {
        $group = Group::findOrFail($request->id);
        $group->name = $request->nama;
        $group->save();

        return response($group, 200);
    }

    /**
     * Untuk delete data group
     * @param $id
     */

    public function delete($id)
    {
        Group::findOrFail($id)->delete();

        return response($id, 200);
    }

    /**
     * Date : 16/10/2021
     * Description : display permission pada grup
     * Developer : Dimas
     * Status : Add
     */

    public function showPrevilage($id)
    {
        $group = Group::where('id', $id)->first();
        $role = Roles::all()->toArray();
        $permission = GroupRoles::where('group_type_id', $id)->get()->toArray();
        $isExists = [];

        for($i = 0; $i < count($role); $i++){
            for($j = 0; $j < count($permission); $j++){
                if($role[$i]['id'] == $permission[$j]['role_id']){
                    $isExists[$j] = $role[$i]['id'];
                    break;
                }
            }
        }
        
        return response()->json(['group' => $group, 'role' => $role, 'isExists' => $isExists]);
    }

    /**
     * Date : 18/10/2021
     * Description : create permission pada grup DB
     * Developer : Dimas
     * Status : Add
     */

    public function createPermission(Request $request)
    {
        $data = [];
        $index = 0;
        $roles = $request->role_id;
        $group_type = $request->group_type;
        $check_roles = GroupRoles::all()->toArray();
        for($i = 0; $i < count($roles); $i++){
            for($j = 0; $j < count($check_roles); $j++){
                if($group_type == $check_roles[$j]['group_type_id'] && $roles[$i] == $check_roles[$j]['role_id']){
                    $isExist = true;
                    break;
                }
                else{
                    $isExist = false;
                }
            }
            if($isExist == false){
                $data[$index] = [
                    'id' => Uuid::uuid4()->getHex(),
                    'group_type_id' => $group_type,
                    'role_id' => $roles[$i],
                    'created_at' => Carbon::now(),
                    'updated_at' => Carbon::now()
                ];
                $index += 1;
            }
        }

        GroupRoles::insert($data);

        return response($data, 201);
    }
}
