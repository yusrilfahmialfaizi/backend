<?php

     namespace App\Http\Controllers;
    
     use Ramsey\Uuid\Uuid;
     use App\Models\Authors;
     use Illuminate\Http\Request;
     use App\Helpers\WaHelper as Helper;

     class AuthorsController extends Controller
     {
          /**
          * Create a new controller instance.
          *
          * @return void
          */
     
          public function showAllAuthors(){
               return response()->json(Authors::all());
          }

          public function showOneAuthor($id)
          {
               return response()->json(Authors::find($id));
          }

          public function create(Request $request)
          {
               $author          = new Authors;
               $author->uuid    = Uuid::uuid4()->getHex();
               $author->name    = $request->name;
               $author->email   = $request->email;
               $author->github  = $request->github;
               $author->twitter = $request->twitter;
               $author->latest_article_published = $request->latest_article_published;
               $author->location = $request->location;
               $author->save();

               return response()->json($author, 201);
          }

          public function update($id, Request $request)
          {
               $author = Authors::findOrFail($id);
               $author->name = $request->name;
               $author->email = $request->email;
               $author->github = $request->github;
               $author->twitter = $request->twitter;
               $author->latest_article_published = $request->latest_article_published;
               $author->location = $request->location;
               $author->save();
               return response()->json($author, 200);
          }

          public function delete($id)
          {
               Authors::findOrFail($id)->delete();
               return response($id, 200);
          }
     }
     
?>