<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Ramsey\Uuid\Uuid;
use App\Models\User;
use App\Models\Group;
use App\Models\GroupRoles;
use App\Models\UserRoles;
use Carbon\Carbon;

class UsersController extends Controller
{
    /**
     * Untuk menampilkan data users
     */

    public function showAllUsers()
    {
        $user = User::all();
        $no = 0;
        $index = 0;
        foreach($user as $u){
            $no += 1;
            $user[$index]['no'] = $no; 
            $index += 1;
        }

        return response()->json($user);
    }

    /**
     * Untuk menampilkan user secara spesifik (satu data user)
     * @params $id
     */

    public function showOneUser($id)
    {
        return response()->json(User::where('uuid', $id)->first());
    }

    /**
     * Date : 13-10-2021, 19-10-2021
     * Description : create new user
     * Developer : Dimas
     * Status : Add, update
     */

    public function create(Request $request)
    {
        // Check user
        if (User::where('email', $request->email)->exists()) {
            return response(500)->with('alert', 'The email you are using already exists');
        }
        else{
            // Insert new user
            $user = new User;
            $user->uuid = Uuid::uuid4()->getHex();
            $user->company_id = $request->company;
            $user->city_id = $request->city;
            $user->name = $request->name;
            $user->email = $request->email;
            $user->password = app('hash')->make($request->password);
            $user->is_admin = $request->isAdmin;
            $user->is_active = $request->isActive;
            $user->contact_id = $request->contact;
            $user->is_email_activate = 1;
            $user->group_id = $request->group;
            $user->save();

            // get id user
            $id_user = User::where('email', $request->email)->get()->toArray();

            // get group roles
            $roles = GroupRoles::where('group_type_id', $request->group)->get()->toArray();

            $user_roles = [];
            for($i = 0; $i < count($roles); $i++){
                $user_roles[$i] = [
                    'id' => Uuid::uuid4()->getHex(),
                    'user_id' => $id_user[0]['uuid'],
                    'role_id' => $roles[$i]['role_id'],
                    'created_at' => Carbon::now(),
                    'updated_at' => Carbon::now()
                ];
            }

            // insert user roles
            UserRoles::insert($user_roles);

            return response($user, 201);
        }
    }

    /**
     * Untuk update data user
     * @params $request, $id
     */

    public function update(Request $request, $id)
    {
        $user = User::where('uuid', $id)->update([
            'name' => $request->name,
            'email' => $request->email,
            'company_id' => $request->company,
            'city_id' => $request->city,
            'is_admin' => $request->isAdmin,
            'is_active' => $request->isActive,
            'contact_id' => $request->contact
        ]);

        return response($user, 200);
    }

    /**
     * Untuk delete data user
     * @params $id
     */

    public function delete($id)
    {
        User::where('uuid', $id)->delete();
        return response($id, 200);
    }

    /**
     * Date : 19-10-2021
     * Description : get data untuk select field ketika create user baru
     * Developer : Dimas
     * Status : Add
     */

    public function getDataSelect()
    {
        $group_db = Group::all()->toArray();

        $group = [];

        for($i = 0; $i < count($group_db); $i++){
            $group[$i]['text'] = $group_db[$i]['name'];
            $group[$i]['value'] = $group_db[$i]['id'];
        }

        return response($group, 200);
    }

    /**
     * Date : 18-10-2021
     * Description : Update fcm token notifikasi di DB 
     * Developer : Yusril
     * Status : Add
     */
    public function update_fcmToken(Request $request, $id)
    {
        $uuid       = $id;
        $fcm_token  = $request->fcm_token;

        $user = User::where('uuid', $uuid)->update([
            'fcm_token' => $fcm_token
        ]);

        return response()->json($uuid, 200);
    }

    /**
     * Date : 18-10-2021
     * Description : Update fcm token notifikasi di DB 
     * Developer : Yusril
     * Status : Add
     */

    public function pushNotif(Request $request)
    {
        $url    = "https://fcm.googleapis.com/fcm/send";
        $headers = [
            'Authorization: key=' . 'AAAAL1a2tnY:APA91bGHZHCgnmsLJCKzOSDDEq8wGowuQ3uuhqpStr5r3CTIVMd0ZYsk51gpsCfLSM8zKIOT_oteltvz9XFINFCdmSe_df3dRoO2T8SEXZ9Avx2JUmtlXuB3hGXMuSXk18zBBcxKC55A', //server key
            'Content-Type: application/json'
        ];

        $field  = [
            "to"    => $request->fcm_token,
            "collapse_key" => "type_a",
            "notification" => [
                "body" => $request->body,
                "title"=> $request->title
            ]
        ];
        $ch = \curl_init();
        \curl_setopt($ch, CURLOPT_URL, $url);
        \curl_setopt($ch, CURLOPT_POST, true);
        \curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        \curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        \curl_setopt($ch, CURLOPT_POSTFIELDS, \json_encode($field));
        $result = \curl_exec($ch);
        \curl_close($ch);

        return response()->json($result);
    }

}
