<?php

    namespace App\Http\Controllers;

    use Illuminate\Http\Request;
    use App\Helpers\WaHelper as Helper;

    class KirimWaController extends Controller
    {
        //
        public function kirimGambar(Request $request)
        {
            try {
               $reqParams = [
               'token'      => getenv('API_TOKEN'),
               'url'        => 'https://api.kirimwa.id/v1/messages',
               'method'     => 'POST',
               'payload'    => json_encode([
                    'message'       => 'https://rioastamal.net/portfolio/img/rioastamal.jpg',
                    'phone_number'  => $request->no_telp,
                    'message_type'  => 'image',
                    'device_id'     => 'samsung-a-11',
                    'caption'       => 'Foto Profil'
               ])
               ];

               $response = Helper::apiKirimWaRequest($reqParams);
               return response()->json($response);
            } catch (Exception $e) {
               return response()->json($e);
            }
        }

        public function kirimPesan(Request $request)
        {
            try {
                $reqParams = [
                        'token' => getenv('API_TOKEN'),
                        'url' => 'https://api.kirimwa.id/v1/messages',
                        'method' => 'POST',
                        'payload' => json_encode([
                            'message' => $request->message,
                            'phone_number' => $request->no_telp,
                            'message_type' => 'text',
                            'device_id' => 'samsung-a-11'
                        ])
                ];

                $response = Helper::apiKirimWaRequest($reqParams);
                return response()->json($response);
            } catch (Exception $e) {
                return response()->json($e);
            }
        }

        public function kirimDocument(Request $request)
        {
            // Lihat apiKirimWaRequest() pada Contoh Integrasi diatas
            try {
                $reqParams = [
                    'token' => getenv('API_TOKEN'),
                    'url' => 'https://api.kirimwa.id/v1/messages',
                    'method' => 'POST',
                    'payload' => json_encode([
                        'message' => 'https://rioastamal.net/files/ebook/dasar2-web-programming-1.0.pdf',
                        'phone_number' => $request->no_telp,
                        'message_type' => 'document',
                        'device_id' => 'samsung-a-11',
                        'caption' => 'ebook.pdf'
                    ])
                ];

                $response = apiKirimWaRequest($reqParams);
                return response()->json($response);
            } catch (Exception $e) {
                return response()->json($e);
            }
        }

    }

?>
