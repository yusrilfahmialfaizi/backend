<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class GroupRoles extends Model
{
    protected $table = 'group_roles';

    protected $primaryKey   = 'id';
    protected $keyType      = 'string';
    public $incrementing    = false;
    protected $fillable     = [
        'group_type_id', 'role_id'
    ];
}
