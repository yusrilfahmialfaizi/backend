<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Roles extends Model
{
    //
    protected $primaryKey   = 'id';
    protected $keyType      = 'string';
    public $incrementing    = false;
}
