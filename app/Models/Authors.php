<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;


class Authors extends Model
{
    use HasFactory;
    //
    protected $primaryKey   = 'uuid';
    protected $keyType      = 'string';
    public $incrementing    = false;
    protected $fillable     = [
        'uuid', 'name', 'email', 'github', 'twitter', 'latest_aricle_published', 'location'
    ];


}
