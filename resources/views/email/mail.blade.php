<!DOCTYPE html>
<html lang="en">
<head>
     <meta charset="UTF-8">
     <meta name="viewport" content="width=device-width, initial-scale=1.0">
     <meta http-equiv="X-UA-Compatible" content="ie=edge">
     <title>Email Activation Account</title>
</head>
<body>
     <p>{{$details['title']}}</p>
     <p>{{$details['header']}}</p>
     <p>{{$details['body']}}</p>
     <a href="{{ $details['link'] }}">Click here</a> 
     <p>{{$details['footer']}}</p>
     <p>Thankyou</p>
</body>
</html>